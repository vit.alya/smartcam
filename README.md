# SmartCameraTest
_____
Приложение создано с для демонстрации работы с камерой. 
Умеет распознавать объекты на экране. 
_____
*AVKit, CoreML, Vision.*
_____
Screenshots: 

![](https://i.ibb.co/Qm7vWwc/image.png)![](https://i.ibb.co/pfmLtrV/image.png)![](https://i.ibb.co/C6JKcqN/image.png)
